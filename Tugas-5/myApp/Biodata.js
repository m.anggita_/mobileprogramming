import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button, ImageBackground } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <ImageBackground source={require('./assets/ya.png')} style={styles.bgcont}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/mila.jpeg')} />
                    </View>
                    <Text style={styles.bio}>Mila Anggita</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Program study : Teknik Informatika </Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>kelas : Pagi A </Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Agama : Islam</Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>TTL : Indramayu, 06 Juni 2000</Text>
                    </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Alamat : Purwakarta</Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Instagram : m.anggita_ </Text>
                </View>
                <View style={styles.bio}>
                    <Text style={styles.bio}>Facebook :-</Text>            
                </View>
                </ImageBackground>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: '#2F4F4F',
        flex:1,
        width: '100%',
        height:'100%',
    },
    bgcont:{
        flex:1,
        resizeMode:'cover',
        justifyContent:'center',
        width:'100%',
        height:'100%',
        alignItems:'center',
      },

    bio:{
        textAlign:"center",
    marginTop:5,
    marginBottom:5,
    fontSize:18,
    fontFamily:"serif",
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 10,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68,
        marginBottom:'5%',
        marginTop:'1%',
    },

});