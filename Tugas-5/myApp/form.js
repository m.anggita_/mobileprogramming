import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style = {styles.container}>
                <TextInput style={styles.inputBox}
                 placeholder='Username/email' 
                 />
                 <TextInput style={styles.inputBox2}
                 placeholder='Password'
                 secureTextEntry={true} 
                 />
                 <TouchableOpacity style={styles.button}>
                     <Text style={styles.buttonText}>Login</Text>
                 </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 1,
          justifyContent: 'center',
          alignItems: 'center'
        },
    inputBox:{
        width:300,
        height:50,
        backgroundColor:'#fff',
        borderRadius: 15,
        paddingHorizontal:10,
        fontSize: 16,
        color:'#000',
        marginTop: '70%',
    },
    inputBox2:{
        width:300,
        height:50,
        backgroundColor:'#fff',
        borderRadius: 15,
        paddingHorizontal:10,
        fontSize: 16,
        color:'#000',
        marginTop: 15,

    },
    button:{
        width: '40%',
        height:40,
        backgroundColor: '#DB7093',
        borderRadius:5,
        marginTop:'40%',
        paddingVertical:10,
        alignItems:'center',
    },
    buttonText: {
        fontSize:16,
        color:'#fff',
        alignItems:'center',
        marginBottom:20,
    },
});
